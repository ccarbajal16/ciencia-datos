# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""
import pandas as pd

iris_filename = 'Data/datasets-uci-iris.csv'

iris_chunks = pd.read_csv(iris_filename, header=None, names=['C1', 'C2', 'C3', 'C4', 'C5'], chunksize=10)
 
for chunk in iris_chunks:
    print ('Shape:', chunk.shape)
    print (chunk,'\n')
